const { createConfig } = require('@edx/frontend-build');

module.exports = createConfig('eslint', {
  rules: {
    'comma-dangle': 'off',
    'function-paren-newline': 'off',
    'implicit-arrow-linebreak': 'off',
    'import/prefer-default-export': 'off',
    'indent': ['error', 4],
    'jsx-quotes': ['error', 'prefer-single'],
    'operator-linebreak': 'off',
    'quotes': ['error', 'single', { 'avoidEscape': true }],
    'react/jsx-curly-newline': 'off',
    'react/jsx-indent': ['error', 4],
    'react/jsx-indent-props': 'off',
  },
});
