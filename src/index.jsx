import 'babel-polyfill';
import 'formdata-polyfill';
import { AppProvider, ErrorPage } from '@edx/frontend-platform/react';
import {
    subscribe,
    initialize,
    APP_INIT_ERROR,
    APP_READY,
    ensureConfig,
    mergeConfig,
    getConfig,
} from '@edx/frontend-platform';
import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Switch } from 'react-router-dom';

import Header, {
    messages as headerMessages,
} from '@edx/frontend-component-header';
import Footer, {
    messages as footerMessages,
} from '@edx/frontend-component-footer';

import { NotFoundPage, SearchPage } from './search';
import { messages } from './i18n';

import './index.scss';
import './assets/favicon.ico';

ensureConfig([
    'SEARCH_FILTER_HIDE_SKILLS',
], 'frontend-app-course-search filter component');

subscribe(APP_READY, () => {
    ReactDOM.render(
        <AppProvider>
            <Header />
            <main>
                <Switch>
                    <Route
                        path={getConfig.PUBLIC_PATH}
                        component={SearchPage}
                    />
                    <Route path='*' component={NotFoundPage} />
                </Switch>
            </main>
            <Footer />
        </AppProvider>,
        document.getElementById('root')
    );
});

subscribe(APP_INIT_ERROR, (error) => {
    ReactDOM.render(
        <ErrorPage message={error.message} />,
        document.getElementById('root')
    );
});

initialize({
    messages: [messages, headerMessages, footerMessages],
    requireAuthenticatedUser: false,
    hydrateAuthenticatedUser: false,
    handlers: {
        config: () => {
            mergeConfig(
                {
                    SUPPORT_URL: process.env.SUPPORT_URL,
                    SEARCH_FILTER_HIDE_SKILLS: process.env.SEARCH_FILTER_HIDE_SKILLS,
                },
                'App loadConfig override handler'
            );
        },
    },
});
