import React from 'react';
import { IntlProvider } from 'react-intl';
import { render } from '@testing-library/react';

global.mountWithIntl = (component) =>
    render(component, {
        wrapper: ({ children }) => (
            <IntlProvider textComponent={React.Fragment} locale='en'>
                {children}
            </IntlProvider>
        ),
    });
