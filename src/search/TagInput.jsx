import React from 'react';
import Autosuggest from 'react-autosuggest';
import { injectIntl, intlShape } from '@edx/frontend-platform/i18n';
import PropTypes from 'prop-types';

import messages from './messages';

let lastId = 0;
const nextAutosuggestId = () => `tag-autosuggest-${lastId++}`;

const getTagSuggestionValue = (tag) => tag.key;

export class TagInputInternal extends React.Component {
    constructor() {
        super();
        this.state = {
            value: '',
            suggestions: [],
        };
        this.autosuggestId = nextAutosuggestId();
        this.onSuggestionsFetch = this.onSuggestionsFetch.bind(this);
        this.onSuggestionsClear = this.onSuggestionsClear.bind(this);
        this.renderTagSuggestion = this.renderTagSuggestion.bind(this);
        this.onSelect = this.onSelect.bind(this);
    }

    onSuggestionsFetch({ value }) {
        const inputVal = value.trim().toLowerCase();
        let suggestions = [];
        if (inputVal.length !== 0) {
            suggestions = this.props.tags.filter(
                (tag) =>
                    tag.key.toLowerCase().slice(0, inputVal.length) === inputVal
            );
            if (suggestions.length === 0) {
                suggestions = [{ nomatch: true }];
            }
        }
        this.setState({
            suggestions,
        });
    }

    onSuggestionsClear() {
        this.setState({
            suggestions: [],
        });
    }

    onChange(_, { newValue }) {
        this.setState({ value: newValue });
    }

    onSelect(_, { suggestion }) {
        if (!suggestion.nomatch) {
            this.props.onSelect(suggestion.key);
        }
        this.setState({ value: '' });
    }

    msg(x, values) {
        return this.props.intl.formatMessage(messages[x], values);
    }

    renderTagSuggestion(tag) {
        if (tag.nomatch) {
            return (
                <div className='inner nomatch'>
                    {this.msg('tag-no-matches')}
                </div>
            );
        }
        return (
            <div className='inner'>
                {tag.key} ({tag.doc_count})
            </div>
        );
    }

    render() {
        const inputProps = {
            value: this.state.value,
            id: this.props.id,
            onChange: this.onChange.bind(this),
            className: 'form-control',
        };

        return (
            <Autosuggest
                id={this.autosuggestId}
                suggestions={this.state.suggestions}
                onSuggestionsFetchRequested={this.onSuggestionsFetch}
                onSuggestionsClearRequested={this.onSuggestionsClear}
                getSuggestionValue={getTagSuggestionValue}
                renderSuggestion={this.renderTagSuggestion}
                inputProps={inputProps}
                highlightFirstSuggestion
                onSuggestionSelected={this.onSelect}
            />
        );
    }
}

TagInputInternal.propTypes = {
    tags: PropTypes.arrayOf(
        PropTypes.shape({
            key: PropTypes.string,
            doc_count: PropTypes.number,
        })
    ).isRequired,
    onSelect: PropTypes.func.isRequired,
    intl: intlShape.isRequired,
    id: PropTypes.string.isRequired,
};

export const TagInput = injectIntl(TagInputInternal);
