import React from 'react';
import { configureAxe, toHaveNoViolations } from 'jest-axe';
import { Filters } from './Filters';

expect.extend(toHaveNoViolations);

const axeFixed = configureAxe({
    rules: {
        // disable this because there is a false positive (I think) in react-autosuggest
        'aria-input-field-name': { enabled: false },
    },
});

test('Filters component', async () => {
    const component = mountWithIntl(
        <Filters
            onChange={(value) => {
                /* empty */
            }}
            roles={['dev']}
            skills={['intermediate']}
            contentTypes={['other']}
            publicationDate='past-year'
            tags={[]}
            aggregations={{
                roles: [
                    {
                        key: 'Architect',
                        doc_count: 4,
                    },
                ],
                skills: [
                    {
                        key: 'Programming',
                        doc_count: 3,
                    },
                    {
                        key: 'Typing',
                        doc_count: 2,
                    },
                ],
                content_type: [
                    {
                        key: 'course',
                        doc_count: 4,
                    },
                ],
                publication_date: [
                    {
                        key: 'Past year',
                        doc_count: 4,
                    },
                    {
                        key: 'Past month',
                        doc_count: 3,
                    },
                    {
                        key: 'Past week',
                        doc_count: 0,
                    },
                ],
                tags: [
                    {
                        key: 'tag1',
                        doc_count: 4,
                    },
                    {
                        key: 'tag2',
                        doc_count: 3,
                    },
                ],
            }}
        />
    );
    expect(component.asFragment()).toMatchSnapshot();
    expect(await axeFixed(component.container)).toHaveNoViolations();
});

test('Filters component with tags', async () => {
    const component = mountWithIntl(
        <Filters
            onChange={(value) => {
                /* empty */
            }}
            roles={[]}
            skills={[]}
            contentTypes={[]}
            publicationDate='past-year'
            tags={['tag1', 'tag3']}
            aggregations={{
                roles: [],
                skills: [],
                content_type: [],
                publication_date: [],
                tags: [
                    {
                        key: 'tag1',
                        doc_count: 4,
                    },
                    {
                        key: 'tag2',
                        doc_count: 3,
                    },
                    {
                        key: 'tag3',
                        doc_count: 1,
                    },
                ],
            }}
        />
    );
    expect(component.asFragment()).toMatchSnapshot();
    expect(await axeFixed(component.container)).toHaveNoViolations();
});
