import React from 'react';

import { getConfig, mergeConfig } from '@edx/frontend-platform';
import {
    getAuthenticatedHttpClient,
    configure,
    MockAuthService,
} from '@edx/frontend-platform/auth';
import MockAdapter from 'axios-mock-adapter';

import { configureAxe, toHaveNoViolations } from 'jest-axe';
import { SearchPage } from './SearchPage';

expect.extend(toHaveNoViolations);

const axeFixed = configureAxe({
    rules: {
        // disable this because there is a false positive (I think) in react-autosuggest
        'aria-input-field-name': { enabled: false },
    },
});

const mockLoggingService = {
    logInfo: jest.fn(),
    logError: jest.fn(),
};
mergeConfig({
    authenticatedUser: {
        userId: 'abc123',
        username: 'Mock User',
        roles: [],
        administrator: false,
    },
});
configure(MockAuthService, {
    config: getConfig(),
    loggingService: mockLoggingService,
});
const mockAdapter = new MockAdapter(getAuthenticatedHttpClient());
// Mock calls for your tests.  This configuration can be done in any sort of test setup.
mockAdapter.onGet('http://localhost:18000/course-search/').reply(200, {
    total: 2,
    size: 2,
    results: [
        {
            name: 'Demonstration Course',
            short_description: 'This is the demo course short description',
            key: 'course-v1:edX+DemoX+Demo_Course',
            role: '',
            skill: '',
            content_type: '',
            invitation_only: false,
        },
        {
            name: 'E2E Test Course',
            short_description: '',
            key: 'course-v1:edX+E2E-101+course',
            role: '',
            skill: '',
            content_type: '',
            invitation_only: false,
        },
    ],
    aggregations: {
        roles: [
            {
                key: 'Architect',
                doc_count: 2,
            },
        ],
        content_type: [],
        skills: [],
        publication_date: [
            {
                key: 'Past year',
                from: 1581311654902.0,
                from_as_string: '2020-02-10T05:14:14.902Z',
                to: 1612934054902.0,
                to_as_string: '2021-02-10T05:14:14.902Z',
                doc_count: 0,
            },
            {
                key: 'Past month',
                from: 1610255654902.0,
                from_as_string: '2021-01-10T05:14:14.902Z',
                to: 1612934054902.0,
                to_as_string: '2021-02-10T05:14:14.902Z',
                doc_count: 0,
            },
            {
                key: 'Past week',
                from: 1612329254902.0,
                from_as_string: '2021-02-03T05:14:14.902Z',
                to: 1612934054902.0,
                to_as_string: '2021-02-10T05:14:14.902Z',
                doc_count: 0,
            },
        ],
        tags: [],
    },
});

const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

test('snapshot test', async () => {
    const component = mountWithIntl(<SearchPage />);

    // wait for the search page to request search results (will receive data from the mocks above)
    await sleep(2000);

    expect(component.asFragment()).toMatchSnapshot();
    expect(await axeFixed(component.container)).toHaveNoViolations();
});
