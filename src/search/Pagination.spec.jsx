import React from 'react';
import { axe, toHaveNoViolations } from 'jest-axe';
import { Pagination } from './Pagination';

expect.extend(toHaveNoViolations);

test('Pagination component on first page, <7 pages', async () => {
    const component = mountWithIntl(
        <Pagination
            onChange={(n) => {
                /* empty */
            }}
            page={1}
            total={5}
        />
    );
    expect(component.asFragment()).toMatchSnapshot();
    expect(await axe(component.container)).toHaveNoViolations();
});

test('Pagination component with ellipsis', async () => {
    const component = mountWithIntl(
        <Pagination
            onChange={(n) => {
                /* empty */
            }}
            page={6}
            total={50}
        />
    );
    expect(component.asFragment()).toMatchSnapshot();
    expect(await axe(component.container)).toHaveNoViolations();
});

test('Pagination component with ellipsis and page 2', async () => {
    const component = mountWithIntl(
        <Pagination
            onChange={(n) => {
                /* empty */
            }}
            page={2}
            total={50}
        />
    );
    expect(component.asFragment()).toMatchSnapshot();
    expect(await axe(component.container)).toHaveNoViolations();
});

test('Pagination component with ellipsis and page max - 1', async () => {
    const component = mountWithIntl(
        <Pagination
            onChange={(n) => {
                /* empty */
            }}
            page={49}
            total={50}
        />
    );
    expect(component.asFragment()).toMatchSnapshot();
    expect(await axe(component.container)).toHaveNoViolations();
});
