import React from 'react';
import { injectIntl, intlShape } from '@edx/frontend-platform/i18n';
import { AppContext } from '@edx/frontend-platform/react';
import { Button, InputSelect } from '@edx/paragon';
import PropTypes from 'prop-types';

import { TagInput } from './TagInput';
import messages from './messages';

let lastId = 0;
const nextTagInputId = () => `tag-input-${lastId++}`;

export const emptyFilters = {
    roles: [],
    skills: [],
    contentTypes: [],
    publicationDate: '',
    tags: [],
};

class FiltersInternal extends React.Component {
    constructor() {
        super();
        this.tagInputId = nextTagInputId();
        this.tagInputOnSelect = this.tagInputOnSelect.bind(this);
    }

    onChange(newValues) {
        this.props.onChange({
            roles: this.props.roles,
            skills: this.props.skills,
            contentTypes: this.props.contentTypes,
            publicationDate: this.props.publicationDate,
            tags: this.props.tags,
            ...newValues,
        });
    }

    onClear() {
        this.props.onChange(emptyFilters);
    }

    onRemoveTag(index) {
        this.onChange({
            tags: this.props.tags.filter((_, i) => i !== index),
        });
    }

    getTagOptions() {
        const values = this.props.aggregations.tags || [];
        return values.filter((x) => !this.props.tags.includes(x.key));
    }

    getOptions(aggregation) {
        const blankOption = [{ value: '', label: this.msg('filter-any') }];
        const values = this.props.aggregations[aggregation] || [];
        return blankOption.concat(
            values.map((x) => ({
                value: x.key,
                label: `${x.key} (${x.doc_count})`,
            }))
        );
    }

    msg(x, values) {
        return this.props.intl.formatMessage(messages[x], values);
    }

    tagInputOnSelect(tag) {
        if (this.props.tags.includes(tag)) {
            return;
        }
        this.onChange({
            tags: this.props.tags.concat(tag),
        });
    }

    renderTag(name, index) {
        return (
            <div className='tag'>
                <button
                    type='button'
                    onClick={() => this.onRemoveTag(index)}
                    label={this.msg('filter-unselect-tag', { name })}
                    aria-label={this.msg('filter-unselect-tag', { name })}
                    title={this.msg('filter-unselect-tag', { name })}
                >
                    ×
                </button>
                {name}
            </div>
        );
    }

    render() {
        const showSkills = !this.context.config.SEARCH_FILTER_HIDE_SKILLS;

        const showClearBtn =
            this.props.roles.length !== 0 ||
            this.props.skills.length !== 0 ||
            this.props.contentTypes.length !== 0 ||
            this.props.publicationDate ||
            this.props.tags.length !== 0;

        // TODO: support multi select for role, skill, contentType,
        //       and remove the `[0] || ''` from InputSelect `value` values.
        //       (this is a temporary hack until we officially support multi select;
        //       multi select is supported on the backend and elsewhere in the frontend)
        return (
            <div className='filters'>
                {showClearBtn && (
                    <Button onClick={() => this.onClear()}>
                        {this.msg('clear-filters-btn')}
                    </Button>
                )}
                <InputSelect
                    name='filter-role'
                    label={this.msg('filter-role')}
                    value={this.props.roles[0] || ''}
                    onChange={(role) =>
                        this.onChange({ roles: role ? [role] : [] })
                    }
                    options={this.getOptions('roles')}
                />

                {showSkills && (
                    <InputSelect
                        name='filter-skill'
                        label={this.msg('filter-skill')}
                        value={this.props.skills[0] || ''}
                        onChange={(skill) =>
                            this.onChange({ skills: skill ? [skill] : [] })
                        }
                        options={this.getOptions('skills')}
                    />
                )}

                <InputSelect
                    name='filter-content-type'
                    label={this.msg('filter-content-type')}
                    value={this.props.contentTypes[0] || ''}
                    onChange={(contentType) =>
                        this.onChange({
                            contentTypes: contentType ? [contentType] : [],
                        })
                    }
                    options={this.getOptions('content_type')}
                />

                <InputSelect
                    name='filter-publicationDate'
                    label={this.msg('filter-publicationDate')}
                    value={this.props.publicationDate}
                    onChange={(publicationDate) =>
                        this.onChange({ publicationDate })
                    }
                    options={this.getOptions('publication_date')}
                />

                <label htmlFor={this.tagInputId}>
                    {this.msg('filter-tag')}
                </label>
                <TagInput
                    id={this.tagInputId}
                    onSelect={this.tagInputOnSelect}
                    tags={this.getTagOptions()}
                />
                <div className='tags'>
                    {this.props.tags.map(this.renderTag.bind(this))}
                </div>
            </div>
        );
    }
}

FiltersInternal.contextType = AppContext;

const aggregationShape = PropTypes.arrayOf(
    PropTypes.shape({
        key: PropTypes.string,
        doc_count: PropTypes.number,
    })
);

FiltersInternal.propTypes = {
    roles: PropTypes.arrayOf(PropTypes.string).isRequired,
    skills: PropTypes.arrayOf(PropTypes.string).isRequired,
    contentTypes: PropTypes.arrayOf(PropTypes.string).isRequired,
    tags: PropTypes.arrayOf(PropTypes.string).isRequired,
    publicationDate: PropTypes.string.isRequired,
    aggregations: PropTypes.shape({
        roles: aggregationShape,
        skills: aggregationShape,
        content_type: aggregationShape,
        publication_date: aggregationShape,
        tags: aggregationShape,
    }).isRequired,
    onChange: PropTypes.func.isRequired,
    intl: intlShape.isRequired,
};

export const Filters = injectIntl(FiltersInternal);
