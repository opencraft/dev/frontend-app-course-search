import { defineMessages } from '@edx/frontend-platform/i18n';

const messages = defineMessages({
    'search-results-loading': {
        id: 'search-results-loading',
        defaultMessage: 'Search results loading...',
        description: 'placeholder text when search results are loading.',
    },
    'search-results-no-results': {
        id: 'search-results-no-results',
        defaultMessage: 'No results!',
        description: 'text shown when no search results for query',
    },
    'search-sort-relevance': {
        id: 'search-sort-relevance',
        defaultMessage: 'Relevance',
        description: 'sort by relevance label',
    },
    'search-sort-pub-date': {
        id: 'search-sort-pub-date',
        defaultMessage: 'Publication Date',
        description: 'sort by publication date label',
    },
    'search-input-placeholder': {
        id: 'search-input-placeholder',
        defaultMessage: 'Search',
        description: 'placeholder text in the search input element',
    },
    'search-heading': {
        id: 'search-heading',
        defaultMessage: 'Discover new courses',
        description: 'text heading to the top left of the page',
    },
    'clear-filters-btn': {
        id: 'clear-filters-btn',
        defaultMessage: 'Clear Search',
        description: 'label for button to clear search filters',
    },
    'filter-unselect-tag': {
        id: 'filter-unselect-tag',
        defaultMessage: 'Unselect tag "{name}"',
        description:
            'a11y label for button to unselect tag from search filters',
    },
    'filter-role': {
        id: 'filter-role',
        defaultMessage: 'Role:',
        description: 'filter by role select label',
    },
    'filter-skill': {
        id: 'filter-skill',
        defaultMessage: 'Skill Level:',
        description: 'filter by skill level select label',
    },
    'filter-content-type': {
        id: 'filter-content-type',
        defaultMessage: 'Content Type:',
        description: 'filter by content type select label',
    },
    'filter-publicationDate': {
        id: 'filter-publicationDate',
        defaultMessage: 'Publication Date:',
        description: 'filter by publicationDate select label',
    },
    'filter-tag': {
        id: 'filter-tag',
        defaultMessage: 'Tag:',
        description: 'filter by tag select label',
    },
    'filter-any': {
        id: 'filter-any',
        defaultMessage: 'Any',
        description: 'Option to allow any in a particular group.',
    },
    'sort-by-label': {
        id: 'sort-by-label',
        defaultMessage: 'Sort by:',
        description: 'label on dropdown to change sort by attribute in search.',
    },
    'next-label': {
        id: 'next-label',
        defaultMessage: 'Next',
        description: 'a11y label on next page button in pagination',
    },
    'previous-label': {
        id: 'previous-label',
        defaultMessage: 'previous',
        description: 'a11y label on previous page button in pagination',
    },
    'pagination-message': {
        id: 'pagination-message',
        defaultMessage: 'Showing {first}-{last} of {total} matches.',
        description: 'message near pagination controls to show stats',
    },
    'tag-no-matches': {
        id: 'tag-no-matches',
        defaultMessage: 'No matches found',
        description: 'message when filtering by tags but there are no matches',
    },
});

export default messages;
