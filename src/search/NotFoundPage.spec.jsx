import React from 'react';

import { axe, toHaveNoViolations } from 'jest-axe';

import { NotFoundPage } from './NotFoundPage';

expect.extend(toHaveNoViolations);

test('NotFoundPage', async () => {
    const component = mountWithIntl(<NotFoundPage />);
    expect(component.asFragment()).toMatchSnapshot();
    expect(await axe(component.container)).toHaveNoViolations();
});
