import React from 'react';
import { injectIntl, intlShape } from '@edx/frontend-platform/i18n';
import PropTypes from 'prop-types';

import messages from './messages';

// Take the current page number, and the total number of pages,
// and return an array containing the page numbers or ellipsis to display in the pagination controls.
// 1-n indicates the page number, 0 indicates ellipsis.
const includePages = (currPage, total) => {
    const pageNumbers = [];
    if (total <= 7) {
        // If 7 or less pages, then show all the pages.
        for (let i = 1; i <= total; i++) {
            pageNumbers.push(i);
        }
    } else {
        // Otherwise, we show the first, last, current, and up to 2 each side of current

        // always show the first page
        pageNumbers.push(1);

        // if there should be ellipsis, show it
        if (currPage > 4) {
            pageNumbers.push(0); // ellipsis
        }

        // show the current page and up to 2 each side,
        // constrained by the first and last pages.
        for (
            let i = Math.max(2, currPage - 2);
            i < Math.min(currPage + 3, total);
            i++
        ) {
            pageNumbers.push(i);
        }

        // show ellipsis if there are still pages between page + 2 and last page
        if (currPage + 3 < total) {
            pageNumbers.push(0); // ellipsis
        }

        // always show the last page
        pageNumbers.push(total);
    }

    return pageNumbers;
};

class PaginationInternal extends React.Component {
    msg(x) {
        return this.props.intl.formatMessage(messages[x]);
    }

    render() {
        const currPage = this.props.page;
        const { total } = this.props;
        const pageNumbers = includePages(currPage, total);

        const numberBtns = pageNumbers.map((i) => {
            if (i === 0) {
                return <div className='page-btn ellipsis'>…</div>;
            }
            return (
                <button
                    type='button'
                    className={`page-btn ${i === currPage ? 'active' : ''}`}
                    onClick={() => this.props.onChange(i)}
                >
                    {i}
                </button>
            );
        });

        return (
            <div className='pagination'>
                <button
                    type='button'
                    className='page-btn end'
                    disabled={currPage <= 1}
                    onClick={() => this.props.onChange(currPage - 1)}
                    aria-label={this.msg('previous-label')}
                >
                    {'<'}
                </button>
                {numberBtns}
                <button
                    type='button'
                    className='page-btn end'
                    disabled={currPage >= total}
                    onClick={() => this.props.onChange(currPage + 1)}
                    aria-label={this.msg('next-label')}
                >
                    {'>'}
                </button>
            </div>
        );
    }
}

PaginationInternal.propTypes = {
    page: PropTypes.number.isRequired,
    total: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired,
    intl: intlShape.isRequired,
};

export const Pagination = injectIntl(PaginationInternal);
