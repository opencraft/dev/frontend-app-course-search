import { AppContext } from '@edx/frontend-platform/react';
import { getConfig, history } from '@edx/frontend-platform';
import React from 'react';
import { injectIntl, intlShape } from '@edx/frontend-platform/i18n';
import { FormControl, SearchField, InputGroup } from '@edx/paragon';
import { getAuthenticatedHttpClient } from '@edx/frontend-platform/auth';

import messages from './messages';
import { Filters, emptyFilters } from './Filters';
import { Pagination } from './Pagination';

const PAGE_PATH = getConfig().PUBLIC_PATH || '/';

const searchResultToCard = (course) => {
    const url = `${getConfig().LMS_BASE_URL}/courses/${course.key}`;
    return (
        <div key={course.key} className='search-result'>
            <h2>
                <a href={url}>{course.name}</a>
            </h2>
            {course.short_description && (
                <p className='card-description'>{course.short_description}</p>
            )}
        </div>
    );
};

// The default sorting changes based on whether there is a search query or not.
// The logic throughout the app allows the user to change the sort order,
// but automatically resets to the default order when adding or clearing a search query.
const getDefaultSort = (q) => (q ? 'relevance' : 'pub-date');

class SearchPageInternal extends React.Component {
    constructor() {
        super();
        this.state = {
            results: [],
            aggregations: {},
            loading: true,
            sort: getDefaultSort(''),
            page: 1,
            size: 1,
            total: 0,
            q: '',
            filters: emptyFilters,
            error: '',
        };
    }

    async componentDidMount() {
        this.readSearchParams();
    }

    onUpdateSearchQuery(newQ) {
        const q = newQ || '';
        let sort;
        if ((!this.state.q && q) || (this.state.q && !q)) {
            sort = getDefaultSort(q);
        } else {
            sort = this.state.sort;
        }
        this.setState(
            {
                q,
                page: 1,
                sort,
            },
            () => {
                this.writeSearchParams();
                this.refreshSearchResults();
            }
        );
    }

    msg(x, values) {
        return this.props.intl.formatMessage(messages[x], values);
    }

    // Read the url search params into state,
    // and refresh search results.
    // This is so we can load the search given the url.
    // (ie. search is saved in url params)
    readSearchParams() {
        const searchParams = new URLSearchParams(window.location.search);

        // TODO: sanitize page to a positive int
        const page = searchParams.get('page');

        const q = searchParams.get('q') || '';
        this.setState(
            {
                q,
                filters: {
                    roles: searchParams.getAll('role[]'),
                    skills: searchParams.getAll('skill[]'),
                    contentTypes: searchParams.getAll('content_type[]'),
                    publicationDate: searchParams.get('pub_date') || '',
                    tags: searchParams.getAll('tag[]'),
                },
                page: parseInt(page || 1, 10),
                sort: searchParams.get('sort') || getDefaultSort(q),
            },
            () => this.refreshSearchResults()
        );
    }

    async refreshSearchResults() {
        this.setState({
            loading: true,
            error: '',
        });
        const showSkills = !this.context.config.SEARCH_FILTER_HIDE_SKILLS;
        const skill = showSkills ? this.state.filters.skills : [];

        const config = {
            params: {
                q: this.state.q,
                pub_date: this.state.filters.publicationDate,
                role: this.state.filters.roles,
                skill,
                content_type: this.state.filters.contentTypes,
                tag: this.state.filters.tags,
                sort: this.state.sort,
                page: this.state.page,
            },
        };

        // See api docs at http://localhost:18381/api-docs/#operations-api-v1_course_runs_list
        // See also https://github.com/open-craft/course-discovery/blob/3e59feed2ce8a75c3f33ec7bfc532f6714aa6dbf/course_discovery/apps/api/v1/views/course_runs.py#L117
        // const url = getConfig().DISCOVERY_API_BASE_URL + '/api/v1/course_runs/';
        const url = `${getConfig().LMS_BASE_URL}/course-search/`;
        try {
            // getAuthenticatedHttpClient() has same api as axios.create().
            // See https://github.com/axios/axios#request-config
            const { data } = await getAuthenticatedHttpClient().get(
                url,
                config
            );
            this.setState({
                results: data.results,
                aggregations: data.aggregations,
                size: parseInt(data.size, 10),
                total: data.total,
                loading: false,
            });
        } catch (error) {
            // eslint-disable-next-line no-console
            console.log(error);
            this.setState({
                loading: false,
                error: error.toString(),
            });
        }
    }

    // Write the current search state to url search params.
    // This is so we can save the current search,
    // and refresh and history works for search results.
    writeSearchParams() {
        const searchParams = new URLSearchParams(window.location.search);

        // reset all the params we care about
        searchParams.delete('q');
        searchParams.delete('publicationDate');
        searchParams.delete('role[]');
        searchParams.delete('skill[]');
        searchParams.delete('content_type[]');
        searchParams.delete('tag[]');
        searchParams.delete('sort');
        searchParams.delete('page');

        // Extra logic here is to keep the url nice and clean.
        if (this.state.q !== '') {
            searchParams.set('q', this.state.q);
        }

        if (this.state.filters.publicationDate !== '') {
            searchParams.set(
                'publicationDate',
                this.state.filters.publicationDate
            );
        }

        if (this.state.sort !== getDefaultSort(this.state.q)) {
            searchParams.set('sort', this.state.sort);
        }

        if (this.state.page !== 1) {
            searchParams.set('page', this.state.page);
        }

        this.state.filters.roles
            .filter((x) => x !== '')
            .forEach((role) => searchParams.append('role[]', role));
        this.state.filters.skills
            .filter((x) => x !== '')
            .forEach((skill) => searchParams.append('skill[]', skill));
        this.state.filters.contentTypes
            .filter((x) => x !== '')
            .forEach((contentType) =>
                searchParams.append('content_type[]', contentType)
            );
        this.state.filters.tags
            .filter((x) => x !== '')
            .forEach((tag) => searchParams.append('tag[]', tag));

        const paramString = searchParams.toString();
        if (paramString) {
            history.push(`${PAGE_PATH}?${searchParams.toString()}`);
        } else {
            history.push(PAGE_PATH);
        }
    }

    renderError(error) {
        return (
            <div className='row'>
                <div className='alert alert-warning' role='alert'>
                    {error}
                </div>
            </div>
        );
    }

    renderSearchResults() {
        if (this.state.loading) {
            return <p>{this.msg('search-results-loading')}</p>;
        }
        if (this.state.results.length > 0) {
            return this.state.results.map(searchResultToCard);
        }
        return <p>{this.msg('search-results-no-results')}</p>;
    }

    render() {
        return (
            <div className='search-page container py-5'>
                {this.state.error && this.renderError(this.state.error)}
                <div className='row'>
                    <div className='col-12 col-md-3 search-heading'>
                        {this.msg('search-heading')}
                    </div>

                    <div className='col-12 col-md-9'>
                        <InputGroup className='search-field-group'>
                            <SearchField
                                className='search-field'
                                placeholder={this.msg('search-input-placeholder')}
                                value={this.state.q}
                                onSubmit={(q) => this.onUpdateSearchQuery(q)}
                                onClear={() => this.onUpdateSearchQuery('')}
                            />
                            <InputGroup className='sort-by-group'>
                                <InputGroup.Prepend>
                                    <InputGroup.Text id='sort-by-label'>
                                        {this.msg('sort-by-label')}
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl
                                    as='select'
                                    aria-label={this.msg('sort-by-label')}
                                    aria-describedby='sort-by-label'
                                    value={this.state.sort}
                                    className='sort-by-select'
                                    onChange={(e) => {
                                        this.setState(
                                            { sort: e.target.value },
                                            () => {
                                                this.writeSearchParams();
                                                this.refreshSearchResults();
                                            }
                                        );
                                    }}
                                >
                                    <option value='relevance'>
                                        {this.msg('search-sort-relevance')}
                                    </option>
                                    <option value='pub-date'>
                                        {this.msg('search-sort-pub-date')}
                                    </option>
                                </FormControl>
                            </InputGroup>
                        </InputGroup>
                    </div>
                </div>

                <div className='row'>
                    <div className='col-12 col-md-3'>
                        <Filters
                            onChange={(filters) => {
                                this.setState({ filters, page: 1 }, () => {
                                    this.writeSearchParams();
                                    this.refreshSearchResults();
                                });
                            }}
                            {...this.state.filters}
                            aggregations={this.state.aggregations}
                        />
                    </div>

                    <div className='col-12 col-md-9 search-results'>
                        {this.renderSearchResults()}

                        <div className='pagination-wrapper'>
                            <div className='results-stats'>
                                {this.msg('pagination-message', {
                                    first:
                                        1 +
                                        (this.state.page - 1) * this.state.size,
                                    last: Math.min(
                                        this.state.page * this.state.size,
                                        this.state.total
                                    ),
                                    total: this.state.total,
                                })}
                            </div>
                            <Pagination
                                page={this.state.page}
                                total={Math.ceil(
                                    this.state.total / this.state.size
                                )}
                                onChange={(page) => {
                                    this.setState({ page }, () => {
                                        this.writeSearchParams();
                                        this.refreshSearchResults();
                                    });
                                }}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

SearchPageInternal.contextType = AppContext;

SearchPageInternal.propTypes = {
    intl: intlShape.isRequired,
};

export const SearchPage = injectIntl(SearchPageInternal);
