# frontend-app-course-search

This is a micro-frontend application for course search.

## Development

### Backend

This requires the [backend search app](https://gitlab.com/opencraft/dev/backend-app-course-search) to be installed in the Open edX devstack.

### Start the development server

In this project, install requirements and start the development server by running:

```
npm install
npm start # The server will run on port 1997
```

Once the dev server is up visit http://localhost:1997.

## Configuration and Deployment

This MFE is configured via node environment variables supplied at build time. See the .env file for the list of required environment variables. Example build syntax with a single environment variable:

```
NODE_ENV=development ACCESS_TOKEN_COOKIE_NAME='edx-jwt-cookie-header-payload' npm run build
```

For more information see [Micro-frontend applications in Open edX](https://github.com/edx/edx-developer-docs/blob/5191e800bf16cf42f25c58c58f983bdaf7f9305d/docs/micro-frontends-in-open-edx.rst>).

### Custom configuration

Environment-set configuration as above that is specific to this app includes:

- `SEARCH_FILTER_HIDE_SKILLS`: set this to a non-empty string to hide and ignore the "Skill Level" search filter.


### Theming

- After installing the dependencies for this npm package, install a package that implements [edx/brand-openedx](https://github.com/edx/brand-openedx).
  This package can contain style overrides, image assets, etc.
- Set the header and footer images in the environment:

```
# header image url
LOGO_URL='https://d2jbzx64heofgk.cloudfront.net/openedx-logos/open-edx-logo-tag.png'
# footer image url
LOGO_TRADEMARK_URL='https://d2jbzx64heofgk.cloudfront.net/openedx-logos/open-edx-logo-tag.png'
```

Then `npm build` to build the frontend, baking in the theme.

## Licence

    frontend-app-course-search
    Copyright (C) 2021 OpenCraft

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
